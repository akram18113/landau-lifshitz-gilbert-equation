import numpy as np
import matplotlib.pyplot as plt

# Constants
gamma = 1.76e11     # gyromagnetic ratio in rad/(s*T)
alpha = 0.1         # Gilbert damping coefficient
Ms = 8.6e5          # saturation magnetization in A/m
H = np.array([0, 0, 1e4])  # uniform external magnetic field in A/m

# Initial conditions
M0 = np.array([0, 0, 1])   # initial magnetization in z direction
t0 = 0
tf = 1e-9

# Time step and number of steps
dt = 1e-12
N = int(tf/dt)

# Fourth-order Runge-Kutta method
def LLG(M, H):
    dMdt = -gamma * np.cross(M, H) - alpha * np.cross(M, np.cross(M, H))
    return dMdt

t = t0
M = M0
M_list = [M]
for i in range(N):
    k1 = dt * LLG(M, H)
    k2 = dt * LLG(M + 0.5*k1, H)
    k3 = dt * LLG(M + 0.5*k2, H)
    k4 = dt * LLG(M + k3, H)
    M += (k1 + 2*k2 + 2*k3 + k4) / 6
    t += dt
    M_list.append(M)

M_array = np.array(M_list)
time_array = np.linspace(t0, tf, N+1)

# Plotting the magnetization dynamics
fig, ax = plt.subplots(figsize=(8,6))
ax.plot(time_array, M_array[:, 0], label='M_x')
ax.plot(time_array, M_array[:, 1], label='M_y')
ax.plot(time_array, M_array[:, 2], label='M_z')
ax.legend()
ax.set_xlabel('Time (s)')
ax.set_ylabel('Magnetization')
plt.show()
